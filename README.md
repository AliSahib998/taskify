# Taskify

Taskify project is task system like jira or other to manage tasks

 # Database
 as database, I use h2 in memory database 

 if you run this application in local, database url is localhost:8080/console
 else https://taskify-ibar.herokuapp.com/console

 # Swagger
 Swagger url is localhost:8080/swagger-ui.html or 
 https://taskify-ibar.herokuapp.com/swagger-ui.html

 # Endpoints
 - POST-localhost:8080/sign-up (for organization and create user in admin role)
 - POST-localhost:8080/auth (for login, return token which you will use other endpoints)
 - POST-localhost:8080/users (user will added only by user who has admin role)
 - GET-localhost:8080/task (return all task for that organization)
 - POST-localhost:8080/task (create new task and assign to other users inside organization)

 # JWT
 when you login with auth endpoint to system then return token,
This token will sended in all request as header parameter (only sign-up except)

`Authorization - 'Bearer your token'`

# Unit test
I used spock framework for unit test

# Send Mail
You can change following parameters in application.properties and please switch ON allow less secure apps in your gmail account
 
`spring.mail.username=your username`

`spring.mail.password=your password`
 
