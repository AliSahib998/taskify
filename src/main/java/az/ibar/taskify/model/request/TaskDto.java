package az.ibar.taskify.model.request;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TaskDto {
    private String title;
    private String description;
    private Long deadlineWithDay;
    private List<Long>userIds;
}
