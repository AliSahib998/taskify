package az.ibar.taskify.model.request;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationDto {
    @NotNull
    private String organizationName;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String address;
}
