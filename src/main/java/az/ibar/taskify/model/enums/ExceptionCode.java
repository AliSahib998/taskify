package az.ibar.taskify.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExceptionCode {
    UNEXPECTED_EXCEPTION("exception.unexpected-exception", "Unexpected exception"),
    USER_ALREADY_REGISTERED("exception.user-registered","user already registered"),
    ORGANIZATION_ALREADY_REGISTERED("exception.organization-registered", "organization already registered"),
    ROLE_NOT_FOUND("exception.role-not-found","role not found"),
    USER_NOT_FOUND("exception.user-not-found","user not found"),
    ORGANIZATION_NOT_FOUND("exception.organization-not-found","organization not found"),
    USER_NOT_INCLUDED("exception.user-not-included","user is not included given organization"),
    INVALID_HEADER("exception.header-exception","header is invalid"),
    ACCESS_DENIED("exception.access-denied","access is denied");
    private String code;
    private String message;
}
