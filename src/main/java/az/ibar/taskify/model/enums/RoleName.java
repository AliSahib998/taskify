package az.ibar.taskify.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleName {
    ADMIN("ADMIN"),
    USER("USER");

    private String roleName;

}
