package az.ibar.taskify.model.error;

public enum ErrorLevel {
    WARNING,
    ERROR
}
