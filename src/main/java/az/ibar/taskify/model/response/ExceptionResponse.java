package az.ibar.taskify.model.response;

import az.ibar.taskify.model.error.ValidationError;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {
    private String message;

    private String code;

    private List<ValidationError> checks;
}