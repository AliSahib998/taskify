package az.ibar.taskify.model.view;

import az.ibar.taskify.model.request.OrganizationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserView {
    private String name;
    private String surname;
    private OrganizationDto organization;
}
