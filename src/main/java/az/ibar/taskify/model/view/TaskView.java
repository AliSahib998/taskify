package az.ibar.taskify.model.view;

import az.ibar.taskify.model.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskView {
    private String title;
    private String description;
    private TaskStatus taskStatus;
    private UserView reporter;
    private List<UserView> assignee;
}
