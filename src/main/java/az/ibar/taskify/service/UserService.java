package az.ibar.taskify.service;

import az.ibar.taskify.model.request.SignUpDto;
import az.ibar.taskify.model.request.UserDto;

public interface UserService {
    void createOrganization(SignUpDto signUpDto);
    void addUser(UserDto userDto, String username);
}
