package az.ibar.taskify.service;

import az.ibar.taskify.model.request.TaskDto;
import az.ibar.taskify.model.view.TaskView;

import java.util.List;

public interface TaskService {
   void createTask(TaskDto taskDto, String username);
   List<TaskView> getAllOrganizationTask(String username);
}
