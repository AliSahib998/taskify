package az.ibar.taskify.service.impl;

import az.ibar.taskify.dao.entity.OrganizationEntity;
import az.ibar.taskify.dao.entity.TaskEntity;
import az.ibar.taskify.dao.entity.UserEntity;
import az.ibar.taskify.dao.repository.TaskRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.exception.OrganizationNotFoundException;
import az.ibar.taskify.exception.UserNotFoundException;
import az.ibar.taskify.mapper.TaskMapper;
import az.ibar.taskify.model.enums.TaskStatus;
import az.ibar.taskify.model.request.TaskDto;
import az.ibar.taskify.model.view.TaskView;
import az.ibar.taskify.service.MailService;
import az.ibar.taskify.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

import static az.ibar.taskify.model.enums.ExceptionCode.ORGANIZATION_NOT_FOUND;
import static az.ibar.taskify.model.enums.ExceptionCode.USER_NOT_FOUND;
import static az.ibar.taskify.model.enums.ExceptionCode.USER_NOT_INCLUDED;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private UserRepository userRepository;
    private MailService mailService;

    public TaskServiceImpl(TaskRepository taskRepository,
                           UserRepository userRepository,
                           MailService mailService) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.mailService = mailService;
    }

    private static final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Override
    public void createTask(TaskDto taskDto, String username) {
        logger.info("ActionLog.createTask.start");
        List<UserEntity> userList = new ArrayList<>();

        //1. check organization
        UserEntity reporter = userRepository.findByEmail(username)
                .orElseThrow(()->{
                    logger.error("ActionLog.createTask.error --unexpected error");
                    throw new IllegalStateException("unexpected error");
                });
        OrganizationEntity organizationEntity = reporter.getOrganizationEntity();

        if (organizationEntity == null) {
            logger.error("ActionLog.createTask.error --organization not found");
            throw new OrganizationNotFoundException(ORGANIZATION_NOT_FOUND.getMessage(),
                    ORGANIZATION_NOT_FOUND.getCode());
        }

        //2. dto map to entity
        TaskEntity taskEntity = TaskMapper.INSTANCE.dtoToEntity(taskDto);
        taskEntity.setTaskStatus(TaskStatus.TODO);
        taskEntity.setReporter(reporter);
        taskEntity.setOrganization(organizationEntity);

        //3. check user ids
        taskDto.getUserIds().forEach(id -> {
            UserEntity userEntity = userRepository.findById(id)
                    .orElseThrow(()-> {
                        logger.error("ActionLog.createTask.error --user not found");
                        throw new UserNotFoundException(USER_NOT_FOUND.getMessage(),
                                USER_NOT_FOUND.getCode());
                    });
            if (!userEntity.getOrganizationEntity().getId().equals(organizationEntity.getId())) {
                logger.error("ActionLog.createTask.error -- user - {} not included to this organization",
                        userEntity.getId());
                throw new UserNotFoundException(USER_NOT_INCLUDED.getMessage(), USER_NOT_INCLUDED.getCode());
            } else {
                userList.add(userEntity);
            }
        });

        taskEntity.getUsers().addAll(userList);

        taskRepository.save(taskEntity);

        mailService.sendMail(userList, taskDto, reporter);

        logger.info("ActionLog.createTask.end");
    }

    @Override
    public List<TaskView> getAllOrganizationTask(String username) {
        logger.info("ActionLog.getAllOrganizationTask.start");

        //1. check organization
        UserEntity userEntity = userRepository.findByEmail(username)
                .orElseThrow(()->{
                    logger.error("ActionLog.getAllOrganizationTask.error --unexpected error");
                    throw new IllegalStateException("unexpected error");
                });
        OrganizationEntity organizationEntity = userEntity.getOrganizationEntity();

        if (organizationEntity == null) {
            logger.error("ActionLog.createTask.error --organization not found");
            throw new OrganizationNotFoundException(ORGANIZATION_NOT_FOUND.getMessage(),
                    ORGANIZATION_NOT_FOUND.getCode());
        }

        List<TaskEntity> tasks = taskRepository.findByOrganization(organizationEntity);

        return TaskMapper.INSTANCE.mapTaskViews(tasks);
    }
}
