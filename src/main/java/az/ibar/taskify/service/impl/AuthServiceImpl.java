package az.ibar.taskify.service.impl;

import az.ibar.taskify.config.security.jwt.JwtTokenUtil;
import az.ibar.taskify.model.request.AuthRequestDto;
import az.ibar.taskify.model.response.AuthResponseDto;
import az.ibar.taskify.service.AuthService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private UserDetailsService userDetailsService;
    private AuthenticationManager authManager;
    private JwtTokenUtil jwtTokenUtil;

    public AuthServiceImpl(@Qualifier("appUserDetailsService") UserDetailsService userDetailsService,
                           AuthenticationManager authManager,
                           JwtTokenUtil jwtTokenUtil) {
        this.userDetailsService = userDetailsService;
        this.authManager = authManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    public AuthResponseDto login(AuthRequestDto authRequestDto) {
        UsernamePasswordAuthenticationToken tokenAuth = new UsernamePasswordAuthenticationToken(
                authRequestDto.getUsername(), authRequestDto.getPassword());

        final Authentication authentication = authManager.authenticate(tokenAuth);

        if (!authentication.isAuthenticated()) {
            throw new BadCredentialsException("Unknown username or password");
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequestDto.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);

        return AuthResponseDto.builder().token(token).build();

    }
}
