package az.ibar.taskify.service.impl;

import az.ibar.taskify.dao.entity.UserEntity;
import az.ibar.taskify.model.request.TaskDto;
import az.ibar.taskify.service.MailService;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Objects;

@Service
public class MailServiceImpl implements MailService {

    private JavaMailSender javaMailSender;

    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);
    private static final String MAIL_TEMPLATE = "template/mail.html";


    public MailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    @Async
    public void sendMail(List<UserEntity> userList, TaskDto taskDto, UserEntity reporter) {

        MimeMessage message = javaMailSender.createMimeMessage();
        userList.forEach(userEntity -> {
            try {
                message.setSubject(taskDto.getTitle());
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                helper.setTo(userEntity.getEmail());
                helper.setText(Objects.requireNonNull(createBody(taskDto, reporter)),true);
                javaMailSender.send(message);
            } catch (Exception ex) {
                logger.error("Error occurred when mail send", ex);
            }
        });
    }

    private String createBody(TaskDto taskDto, UserEntity reporter)  {
        try {
            return Resources.toString(Resources.getResource(MAIL_TEMPLATE), Charsets.UTF_8)
                    .replace("{{ name }}", reporter.getName())
                    .replace("{{ surname }}", reporter.getSurname())
                    .replace("{{ description }}", taskDto.getDescription())
                    .replace("{{ deadline }}", taskDto.getDeadlineWithDay() +" day");
        } catch (Exception ex) {
            logger.error("Error in generating HTML", ex);
            return null;
        }

    }

}
