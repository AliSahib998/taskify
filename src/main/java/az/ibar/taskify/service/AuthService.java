package az.ibar.taskify.service;

import az.ibar.taskify.model.request.AuthRequestDto;
import az.ibar.taskify.model.response.AuthResponseDto;

public interface AuthService {
    AuthResponseDto login(AuthRequestDto authRequestDto);
}
