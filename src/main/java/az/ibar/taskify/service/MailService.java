package az.ibar.taskify.service;

import az.ibar.taskify.dao.entity.UserEntity;
import az.ibar.taskify.model.request.TaskDto;

import java.util.List;

public interface MailService {
    void sendMail(List<UserEntity> userList, TaskDto taskDto, UserEntity reporter);
}
