package az.ibar.taskify.controller;

import az.ibar.taskify.model.request.TaskDto;
import az.ibar.taskify.model.view.TaskView;
import az.ibar.taskify.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }


    @PostMapping
    public ResponseEntity<Void> createTask(@RequestBody TaskDto taskDto) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        taskService.createTask(taskDto,user.getUsername());
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public List<TaskView> getAllTasks() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return taskService.getAllOrganizationTask(user.getUsername());
    }

}
