package az.ibar.taskify.controller;

import az.ibar.taskify.model.request.SignUpDto;
import az.ibar.taskify.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
public class RegistrationController {
    private UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Valid SignUpDto signUpDto) {
        userService.createOrganization(signUpDto);
        return ResponseEntity.ok().build();
    }
}
