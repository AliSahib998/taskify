package az.ibar.taskify.controller;

import az.ibar.taskify.exception.AuthException;
import az.ibar.taskify.exception.InvalidHeaderException;
import az.ibar.taskify.exception.OrganizationNotFoundException;
import az.ibar.taskify.exception.OrganizationRegisteredException;
import az.ibar.taskify.exception.RoleNotFoundException;
import az.ibar.taskify.exception.UserNotFoundException;
import az.ibar.taskify.exception.UserRegisteredException;
import az.ibar.taskify.model.error.ErrorLevel;
import az.ibar.taskify.model.error.ValidationError;
import az.ibar.taskify.model.response.ExceptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ErrorHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handle(Exception ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
                "UNEXPECTED_EXCEPTION", null);
    }

    @ExceptionHandler(InvalidHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleInvalidHeaderException(InvalidHeaderException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ExceptionResponse handleAuthException(AuthException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleUserNotFoundException(UserNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(OrganizationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleOrganizationNotFoundException(OrganizationNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(OrganizationRegisteredException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleUserNotFoundException(OrganizationRegisteredException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }


    @ExceptionHandler(RoleNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionResponse handleRoleNotFoundException(RoleNotFoundException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(UserRegisteredException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleOrganizationNotFoundException(UserRegisteredException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                ex.getCode(), null);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleBadCredentialsException(BadCredentialsException ex) {
        log.error("Exception ", ex);
        return new ExceptionResponse(ex.getMessage(),
                "Bad credentials", null);
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        log.error("Validated exception: {}", ex);

        List<ValidationError> checks = new ArrayList<>();
        BindingResult bindingResult = ex.getBindingResult();
        checks.addAll(
                bindingResult
                        .getFieldErrors()
                        .stream()
                        .map(fieldError -> new ValidationError(
                                ErrorLevel.ERROR,
                                fieldError.getField(),
                                fieldError.getDefaultMessage()
                        ))
                        .collect(Collectors.toList())
        );
        checks.addAll(
                bindingResult
                        .getGlobalErrors()
                        .stream()
                        .map(globalError -> new ValidationError(
                                ErrorLevel.ERROR,
                                globalError.getObjectName(),
                                globalError.getDefaultMessage()
                        ))
                        .collect(Collectors.toList())
        );

        return new ResponseEntity<>(
                new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        checks),
                headers,
                HttpStatus.BAD_REQUEST
        );
    }

}
