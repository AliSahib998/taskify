package az.ibar.taskify.exception;

import lombok.Getter;

@Getter
public class InvalidHeaderException extends RuntimeException {
    private String code;
    private String message;

    public InvalidHeaderException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
