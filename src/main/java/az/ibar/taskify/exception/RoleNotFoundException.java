package az.ibar.taskify.exception;

import lombok.Getter;

@Getter
public class RoleNotFoundException extends RuntimeException {
    private String code;
    private String message;

    public RoleNotFoundException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
