package az.ibar.taskify.exception;

import lombok.Getter;

@Getter
public class OrganizationNotFoundException extends RuntimeException {
    private String code;
    private String message;

    public OrganizationNotFoundException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
