package az.ibar.taskify.exception;

import lombok.Getter;

@Getter
public class OrganizationRegisteredException extends RuntimeException {
    private String code;
    private String message;

    public OrganizationRegisteredException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
