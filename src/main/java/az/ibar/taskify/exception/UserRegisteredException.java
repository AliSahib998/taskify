package az.ibar.taskify.exception;

import lombok.Getter;

@Getter
public class UserRegisteredException extends RuntimeException {
    private String code;
    private String message;

    public UserRegisteredException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
