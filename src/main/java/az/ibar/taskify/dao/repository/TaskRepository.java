package az.ibar.taskify.dao.repository;

import az.ibar.taskify.dao.entity.OrganizationEntity;
import az.ibar.taskify.dao.entity.TaskEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<TaskEntity, Long> {
    List<TaskEntity> findByOrganization(OrganizationEntity organizationEntity);
}
