package az.ibar.taskify.dao.repository;

import az.ibar.taskify.dao.entity.OrganizationEntity;
import az.ibar.taskify.dao.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganizationRepository extends CrudRepository<OrganizationEntity, Long> {
    Optional<OrganizationEntity> findByOrganizationName(String organizationName);
}
