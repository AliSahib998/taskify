package az.ibar.taskify.dao.entity;

import az.ibar.taskify.model.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tasks")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "task_status")
    @Enumerated(value = EnumType.STRING)
    private TaskStatus taskStatus;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "reporter_id", referencedColumnName = "id")
    private UserEntity reporter;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "organization_id", referencedColumnName = "id")
    private OrganizationEntity organization;

    @ManyToMany
    @JoinTable(name = "users_tasks", joinColumns
            = @JoinColumn(name = "task_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id",
                    referencedColumnName = "id"))
    private List<UserEntity> users = new ArrayList<>();

    @Column(name = "deadline")
    private LocalDateTime deadline;

    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;
}
