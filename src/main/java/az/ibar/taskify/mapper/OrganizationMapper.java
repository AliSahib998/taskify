package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.OrganizationEntity;
import az.ibar.taskify.model.request.OrganizationDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class OrganizationMapper {
    public static final OrganizationMapper INSTANCE = Mappers.getMapper(OrganizationMapper.class);

    public abstract OrganizationEntity dtoToEntity(OrganizationDto organizationDto);

    public abstract OrganizationDto entityToDto(OrganizationEntity organizationEntity);
}
