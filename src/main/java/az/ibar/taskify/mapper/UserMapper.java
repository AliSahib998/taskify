package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.UserEntity;
import az.ibar.taskify.model.request.SignUpDto;
import az.ibar.taskify.model.request.UserDto;
import az.ibar.taskify.model.view.UserView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = OrganizationMapper.class)
public abstract class UserMapper {
    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "organizationEntity",source = "signUpDto.organization")
    public abstract UserEntity signUpDtoToEntity(SignUpDto signUpDto);

    public abstract UserEntity userDtoToEntity(UserDto userDto);

    @Mapping(target = "organization",source = "userEntity.organizationEntity")
    public abstract UserView mapView(UserEntity userEntity);

    public abstract List<UserView> mapViews(List<UserEntity>userEntities);
}
