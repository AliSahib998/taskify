package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.TaskEntity;
import az.ibar.taskify.model.request.TaskDto;
import az.ibar.taskify.model.view.TaskView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import java.time.LocalDateTime;
import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,uses = {UserMapper.class, OrganizationMapper.class})
public abstract class TaskMapper {
    public static final TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    @Mapping(target = "deadline", source = "taskDto.deadlineWithDay", qualifiedByName = "dayConvertToDate")
    public abstract TaskEntity dtoToEntity(TaskDto taskDto);

    @Named("dayConvertToDate")
    LocalDateTime mapDeadline(Long deadlineWithDay) {
        return LocalDateTime.now().plusDays(deadlineWithDay);
    }

    @Mappings({
            @Mapping(target = "reporter", source = "taskEntity.reporter"),
            @Mapping(target = "assignee", source = "taskEntity.users")
    })
    public abstract TaskView mapTaskView(TaskEntity taskEntity);

    public abstract List<TaskView> mapTaskViews(List<TaskEntity> taskEntities);
}
