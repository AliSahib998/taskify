package az.ibar.taskify.config.security.filter;

import az.ibar.taskify.config.security.jwt.JwtTokenUtil;
import az.ibar.taskify.exception.AuthException;
import az.ibar.taskify.exception.InvalidHeaderException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static az.ibar.taskify.model.enums.ExceptionCode.ACCESS_DENIED;
import static az.ibar.taskify.model.enums.ExceptionCode.INVALID_HEADER;

public class AuthenticationTokenFilter extends OncePerRequestFilter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	@Qualifier("appUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	@Qualifier("handlerExceptionResolver")
	private HandlerExceptionResolver resolver;

	private final String tokenHeader = "Authorization";

	private static final String[] AUTH_WHITELIST = {
			"v2/api-docs",
			"swagger-resources",
			"swagger-resources",
			"configuration/ui",
			"configuration/security",
			"swagger-ui.html",
			"webjars",
			"v3/api-docs",
			"swagger-ui",
			"auth",
			"sign-up",
			"console",
	};

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		
		final String requestHeader = request.getHeader(this.tokenHeader);



		String username;
		String authToken;
		boolean isPermit = false;

		 try {

			 for (String s : AUTH_WHITELIST) {
				 if (request.getServletPath().contains(s)) {
					 isPermit = true;
					 break;
				 }
			 }

		 	 if (isPermit) {
		 	 	chain.doFilter(request,response);
			 } else {

				 if ((requestHeader != null && requestHeader.startsWith("Bearer "))) {
					 authToken = requestHeader.substring(7);
					 try {
						 username = jwtTokenUtil.getUsernameFromToken(authToken);
					 } catch (MalformedJwtException e) {
						 logger.error("an error occurred during getting username from token", e);
						 throw new AuthException(ACCESS_DENIED.getMessage(),ACCESS_DENIED.getCode());

					 } catch (ExpiredJwtException e) {
						 logger.warn("the token is expired and not valid anymore", e);
						 throw new AuthException(ACCESS_DENIED.getMessage(),ACCESS_DENIED.getCode());
					 }
				 }

				 else {
					 throw new InvalidHeaderException(INVALID_HEADER.getMessage(),INVALID_HEADER.getCode());
				 }

				 logger.info(" checking authentication for user {} ", username);
				 if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

					 UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

					 if (jwtTokenUtil.validateToken(authToken, userDetails)) {
						 UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
						 authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
						 logger.info("authenticated user " + username + ", setting security context");
						 SecurityContextHolder.getContext().setAuthentication(authentication);
					 }
				 }
				 chain.doFilter(request, response);
			 }

	        }
		 catch (RuntimeException ex) {
			 resolver.resolveException(request, response, null, ex);
		 }
	}

}