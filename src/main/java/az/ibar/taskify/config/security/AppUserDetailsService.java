package az.ibar.taskify.config.security;

import az.ibar.taskify.dao.entity.UserEntity;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.exception.UserNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

import static az.ibar.taskify.model.enums.ExceptionCode.USER_NOT_FOUND;

@Component
public class AppUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public AppUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(s)
                .orElseThrow(()->new UserNotFoundException(USER_NOT_FOUND.getMessage(), USER_NOT_FOUND.getCode()));

        List<GrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority(userEntity.getRole().getRoleName()));

        return new User(userEntity.getEmail(), userEntity.getPassword(), authorities);
    }
}
