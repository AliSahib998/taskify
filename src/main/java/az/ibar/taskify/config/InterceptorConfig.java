package az.ibar.taskify.config;

import az.ibar.taskify.util.HeaderKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

import static az.ibar.taskify.util.MdcParameter.OPERATION;
import static az.ibar.taskify.util.MdcParameter.REQUEST_ID;
import static az.ibar.taskify.util.MdcParameter.USER_AGENT;
import static az.ibar.taskify.util.MdcParameter.USER_IP;

@Component
public class InterceptorConfig extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(InterceptorConfig.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        log.trace("InterceptorConfig.preHandle start");
        MDC.put(OPERATION, request.getRequestURI());
        MDC.put(USER_IP, getHeaderValue(HeaderKey.HEADER_USER_IP, request));
        MDC.put(USER_AGENT, getHeaderValue(HeaderKey.HEADER_USER_AGENT, request));

        String requestId = request.getHeader(HeaderKey.HEADER_REQUEST_ID);
        if (requestId == null) {
            requestId = UUID.randomUUID().toString();
        }

        MDC.put(REQUEST_ID, requestId);

        log.trace("InterceptorConfig.preHandle end");

        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView modelAndView) throws Exception {
        MDC.remove(OPERATION);
        MDC.remove(USER_IP);
        MDC.remove(USER_AGENT);
        MDC.remove(REQUEST_ID);

        super.postHandle(request, response, handler, modelAndView);
    }

    private String getHeaderValue(String headerName,
                                  HttpServletRequest servletRequest) {
        if (servletRequest.getHeaderNames() == null) {
            return null;
        }
        return Collections.list(servletRequest.getHeaderNames())
                .stream()
                .filter(i -> Objects.nonNull(i) && headerName.equalsIgnoreCase(i))
                .findFirst()
                .map(servletRequest::getHeader)
                .orElse(null);
    }
}
