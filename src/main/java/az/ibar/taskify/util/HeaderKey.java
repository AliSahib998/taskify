package az.ibar.taskify.util;

public abstract class HeaderKey {
    public static final String HEADER_REQUEST_ID = "requestid";
    public static final String HEADER_USER_IP = "X-Forwarded-For";
    public static final String HEADER_USER_AGENT = "User-Agent";

    private HeaderKey() {
    }
}