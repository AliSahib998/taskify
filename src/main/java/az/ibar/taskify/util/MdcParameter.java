package az.ibar.taskify.util;

public abstract class MdcParameter {
    public static final String REQUEST_ID = "REQUEST_ID";
    public static final String OPERATION = "OPERATION";
    public static final String USER_IP = "USER_IP";
    public static final String USER_AGENT = "USER_AGENT";

    private MdcParameter() {
    }
}
