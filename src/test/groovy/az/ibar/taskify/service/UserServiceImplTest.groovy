package az.ibar.taskify.service

import az.ibar.taskify.dao.entity.OrganizationEntity
import az.ibar.taskify.dao.entity.RoleEntity
import az.ibar.taskify.dao.entity.UserEntity
import az.ibar.taskify.dao.repository.OrganizationRepository
import az.ibar.taskify.dao.repository.RoleRepository
import az.ibar.taskify.dao.repository.UserRepository
import az.ibar.taskify.exception.OrganizationNotFoundException
import az.ibar.taskify.exception.OrganizationRegisteredException
import az.ibar.taskify.exception.RoleNotFoundException
import az.ibar.taskify.exception.UserRegisteredException
import az.ibar.taskify.model.enums.RoleName
import az.ibar.taskify.model.request.OrganizationDto
import az.ibar.taskify.model.request.SignUpDto
import az.ibar.taskify.model.request.UserDto
import az.ibar.taskify.service.impl.UserServiceImpl
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import spock.lang.Specification

class UserServiceImplTest extends Specification {

    private UserRepository userRepository = Mock()
    private OrganizationRepository organizationRepository = Mock()
    private RoleRepository roleRepository = Mock()
    private BCryptPasswordEncoder bCryptPasswordEncoder = Mock()
    private UserService userService

    def "setup" () {
        userService = new UserServiceImpl(userRepository,
                organizationRepository,
                roleRepository,
                bCryptPasswordEncoder)
    }

    def "createOrganization success" () {
        given:
        OrganizationDto organization = OrganizationDto.builder()
                .organizationName("Test")
                .phoneNumber("+1212111").build()

        SignUpDto signUpDto = SignUpDto.builder()
                .name("Ali")
                .password("123456")
                .email("alisahib909@gmail.com")
                .surname("Sahib")
                .organization(organization).build()

        RoleEntity roleEntity = RoleEntity.builder()
                .id(1)
                .roleName("ADMIN").build()
        when:
        userService.createOrganization(signUpDto)

        then:
        1 * userRepository.findByEmail(signUpDto.email) >> Optional.empty()
        1 * organizationRepository.findByOrganizationName(signUpDto.organization.organizationName) >> Optional.empty()
        1 * roleRepository.findByRoleName(RoleName.ADMIN.name()) >> Optional.of(roleEntity)
        1 * bCryptPasswordEncoder.encode(signUpDto.password) >> "test"
        1 * userRepository.save(_ as UserEntity) >> {
            it.name == "Ali"
            it.password == "Test"
            it.email == "alisahib909@gmail.com"
            it.organizationEntity.organizationName == "Test"
        }
    }

    def "createOrganization error -- user is already registered" () {
        given:
        OrganizationDto organization = OrganizationDto.builder()
                .organizationName("Test")
                .phoneNumber("+1212111").build()

        SignUpDto signUpDto = SignUpDto.builder()
                .name("Ali")
                .password("123456")
                .email("alisahib909@gmail.com")
                .surname("Sahib")
                .organization(organization).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Test")
                .name("Test")
                .id(2)
                .email("ali.sahib.98@mail.ru").build()

        when:
        userService.createOrganization(signUpDto)

        then:
        1 * userRepository.findByEmail(signUpDto.email) >> Optional.of(userEntity)
        UserRegisteredException ex = thrown()
        ex.message == "user already registered"
        ex.code == "exception.user-registered"
    }

    def "createOrganization error -- organization is already current" () {
        given:
        OrganizationDto organization = OrganizationDto.builder()
                .organizationName("Test")
                .phoneNumber("+1212111").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        SignUpDto signUpDto = SignUpDto.builder()
                .name("Ali")
                .password("123456")
                .email("alisahib909@gmail.com")
                .surname("Sahib")
                .organization(organization).build()

        RoleEntity roleEntity = RoleEntity.builder()
                .id(1)
                .roleName("ADMIN").build()
        when:
        userService.createOrganization(signUpDto)

        then:
        1 * userRepository.findByEmail(signUpDto.email) >> Optional.empty()
        1 * organizationRepository.findByOrganizationName(signUpDto.organization.organizationName) >> Optional.of(organizationEntity)
        OrganizationRegisteredException ex = thrown()
        ex.code == "exception.organization-registered"
        ex.message == "organization already registered"
    }

    def "createOrganization error -- role not found" () {
        given:
        OrganizationDto organization = OrganizationDto.builder()
                .organizationName("Test")
                .phoneNumber("+1212111").build()

        SignUpDto signUpDto = SignUpDto.builder()
                .name("Ali")
                .password("123456")
                .email("alisahib909@gmail.com")
                .surname("Sahib")
                .organization(organization).build()

        when:
        userService.createOrganization(signUpDto)

        then:
        1 * userRepository.findByEmail(signUpDto.email) >> Optional.empty()
        1 * organizationRepository.findByOrganizationName(signUpDto.organization.organizationName) >> Optional.empty()
        1 * roleRepository.findByRoleName(RoleName.ADMIN.name()) >> Optional.empty()
        RoleNotFoundException ex = thrown()
        ex.message == "role not found"
        ex.code == "exception.role-not-found"
    }

    def "addUser success" () {
        given:
        UserDto userDto = UserDto.builder()
                .name("Ali")
                .surname("Sahib")
                .email("ali.sahib.98@mail.ru")
                .password("123456").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        RoleEntity roleEntity = RoleEntity.builder()
                .roleName("USER").build()

        def username = "alisahib909@gmail.com"

        when:
        userService.addUser(userDto,username)

        then:
        1 * userRepository.findByEmail(userDto.email) >> Optional.empty()
        1 * userRepository.findByEmail(username) >> Optional.of(userEntity)
        1 * roleRepository.findByRoleName(RoleName.USER.name()) >> Optional.of(roleEntity)
        1 * bCryptPasswordEncoder.encode(userDto.password) >> "test"
        1 * userRepository.save(_ as UserEntity) >> {
            it.name == "Ali"
            it.surname == "Sahib"
            it.email == "ali.sahib.98@mail.ru"
            it.password == "test"
            it.role.roleName == "USER"
        }
    }

    def "addUser error -- user is present" () {
        given:
        UserDto userDto = UserDto.builder()
                .name("Ali")
                .surname("Sahib")
                .email("ali.sahib.98@mail.ru")
                .password("123456").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        def username = "alisahib909@gmail.com"

        when:
        userService.addUser(userDto,username)

        then:
        1 * userRepository.findByEmail(userDto.email) >> Optional.of(userEntity)
        UserRegisteredException ex = thrown()
        ex.message == "user already registered"
        ex.code == "exception.user-registered"
    }

    def "addUser error -- unexpected" () {
        given:
        UserDto userDto = UserDto.builder()
                .name("Ali")
                .surname("Sahib")
                .email("ali.sahib.98@mail.ru")
                .password("123456").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        def username = "alisahib909@gmail.com"

        when:
        userService.addUser(userDto,username)

        then:
        1 * userRepository.findByEmail(userDto.email) >> Optional.empty()
        1 * userRepository.findByEmail(username) >> Optional.empty()
        IllegalStateException ex = thrown()
        ex.message == "unexpected error"
    }


    def "addUser error -- organization is null" () {
        given:
        UserDto userDto = UserDto.builder()
                .name("Ali")
                .surname("Sahib")
                .email("ali.sahib.98@mail.ru")
                .password("123456").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com").build()

        def username = "alisahib909@gmail.com"

        when:
        userService.addUser(userDto,username)

        then:
        1 * userRepository.findByEmail(userDto.email) >> Optional.empty()
        1 * userRepository.findByEmail(username) >> Optional.of(userEntity)
        OrganizationNotFoundException ex = thrown()
        ex.message == "organization not found"
        ex.code == "exception.organization-not-found"
    }

    def "addUser error -- role is not found" () {
        given:
        UserDto userDto = UserDto.builder()
                .name("Ali")
                .surname("Sahib")
                .email("ali.sahib.98@mail.ru")
                .password("123456").build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        def username = "alisahib909@gmail.com"

        when:
        userService.addUser(userDto,username)

        then:
        1 * userRepository.findByEmail(userDto.email) >> Optional.empty()
        1 * userRepository.findByEmail(username) >> Optional.of(userEntity)
        1 * roleRepository.findByRoleName(RoleName.USER.name()) >> Optional.empty()
        RoleNotFoundException ex = thrown()
        ex.message == "role not found"
        ex.code == "exception.role-not-found"
    }





}
