package az.ibar.taskify.service

import az.ibar.taskify.dao.entity.OrganizationEntity
import az.ibar.taskify.dao.entity.TaskEntity
import az.ibar.taskify.dao.entity.UserEntity
import az.ibar.taskify.dao.repository.TaskRepository
import az.ibar.taskify.dao.repository.UserRepository
import az.ibar.taskify.exception.OrganizationNotFoundException
import az.ibar.taskify.exception.UserNotFoundException
import az.ibar.taskify.model.enums.TaskStatus
import az.ibar.taskify.model.request.TaskDto
import az.ibar.taskify.service.impl.TaskServiceImpl
import spock.lang.Specification

class TaskServiceImplTest extends Specification {

    private TaskRepository taskRepository = Mock()
    private UserRepository userRepository = Mock()
    private MailService mailService = Mock()
    private TaskService taskService

    def "setup" () {
        taskService = new TaskServiceImpl(taskRepository, userRepository, mailService)
    }

    def "createTask success" () {
        given:
        TaskDto taskDto = TaskDto.builder()
                .title("title")
                .description("description")
                .deadlineWithDay(5)
                .userIds([1L,2L]).build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity reporter = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Test")
                .name("Test")
                .id(2)
                .email("ali.sahib.98@mail.ru")
                .organizationEntity(organizationEntity).build()

        def  username = "alisahib909@gmail.com"

        when:
        taskService.createTask(taskDto, username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(reporter)
        2 * userRepository.findById(_ as Long) >> Optional.of(userEntity)
        1 * taskRepository.save(_ as TaskEntity) >> {
            it.title == "title"
            it.taskStatus == TaskStatus.TODO
            it.description == "description"
            it.reporter.name == "Ali"
            it.reporter.email == "alisahib909@gmail.com"
            it.reporter.id == 1
            it.organization.id == 1
            it.users.contains(userEntity)
        }
        1 * mailService.sendMail(_ as List, taskDto, reporter)
    }

    def "createTask error -- reporter not found" () {
        given:
        TaskDto taskDto = TaskDto.builder()
                .title("title")
                .description("description")
                .deadlineWithDay(5)
                .userIds([1L,2L]).build()

        def  username = "alisahib909@gmail.com"

        when:
        taskService.createTask(taskDto, username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.empty()
        IllegalStateException ex = thrown()
        ex.message == "unexpected error"
    }

    def "createTask error -- organization not found" () {
        given:
        TaskDto taskDto = TaskDto.builder()
                .title("title")
                .description("description")
                .deadlineWithDay(5)
                .userIds([1L,2L]).build()

        UserEntity reporter = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com").build()

        def  username = "alisahib909@gmail.com"

        when:
        taskService.createTask(taskDto, username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(reporter)
        OrganizationNotFoundException ex = thrown()
        ex.message == "organization not found"
        ex.code == "exception.organization-not-found"
    }

    def "createTask error -- assignee user is not found" () {
        given:
        TaskDto taskDto = TaskDto.builder()
                .title("title")
                .description("description")
                .deadlineWithDay(5)
                .userIds([1L,2L]).build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity reporter = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        def  username = "alisahib909@gmail.com"

        when:
        taskService.createTask(taskDto, username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(reporter)
        1 * userRepository.findById(_ as Long) >> Optional.empty()
        UserNotFoundException ex = thrown()
        ex.message == "user not found"
        ex.code == "exception.user-not-found"
    }

    def "createTask error -- user is not belongs to organization " () {
        given:
        TaskDto taskDto = TaskDto.builder()
                .title("title")
                .description("description")
                .deadlineWithDay(5)
                .userIds([1L,2L]).build()

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity reporter = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Test")
                .name("Test")
                .id(2)
                .email("ali.sahib.98@mail.ru")
                .organizationEntity(OrganizationEntity.builder().id(2).build()).build()

        def  username = "alisahib909@gmail.com"

        when:
        taskService.createTask(taskDto, username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(reporter)
        1 * userRepository.findById(_ as Long) >> Optional.of(userEntity)
        UserNotFoundException ex = thrown()
        ex.message == "user is not included given organization"
        ex.code == "exception.user-not-included"
    }

    def "getAllOrganizationTask success" () {
        given:
        def username = "alisahib909@gmail.com"

        OrganizationEntity organizationEntity = OrganizationEntity.builder()
                .id(1).build()

        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com")
                .organizationEntity(organizationEntity).build()

        TaskEntity taskEntity = TaskEntity.builder()
                .id(1)
                .reporter(userEntity)
                .description("description")
                .title("title")
                .organization(organizationEntity)
                .taskStatus(TaskStatus.TODO).build()

        when:
        def actual = taskService.getAllOrganizationTask(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(userEntity)
        1 * taskRepository.findByOrganization(organizationEntity) >> [taskEntity]
        actual[0].title == taskEntity.title
        actual[0].description == taskEntity.description
        actual[0].reporter.name == taskEntity.reporter.name
        actual[0].reporter.surname == taskEntity.reporter.surname
        actual[0].taskStatus == taskEntity.taskStatus
    }

    def "getAllOrganizationTask error" () {
        given:
        def username = "alisahib909@gmail.com"

        when:
        taskService.getAllOrganizationTask(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.empty()
        IllegalStateException ex = thrown()
        ex.message == "unexpected error"
    }

    def "getAllOrganizationTask error -- organization is null" () {
        given:
        def username = "alisahib909@gmail.com"


        UserEntity userEntity = UserEntity.builder()
                .surname("Sahib")
                .name("Ali")
                .id(1)
                .email("alisahib909@gmail.com").build()

        when:
        taskService.getAllOrganizationTask(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(userEntity)
        OrganizationNotFoundException ex = thrown()
        ex.message == "organization not found"
        ex.code == "exception.organization-not-found"
    }
}
